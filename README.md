This is your new Play application
=================================

This file will be packaged with your application, when using `activator dist`.

[![Circle CI](https://circleci.com/gh/somoso/play-framework-basic-app.svg?style=svg)](https://circleci.com/gh/somoso/play-framework-basic-app)
