package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready... \n\nWell, sort of, you haven't done much to it just yet..."));
    }

}
